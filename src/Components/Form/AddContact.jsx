/* eslint-disable react/prop-types */
function AddContact(props) {
    return (
        <div className="shadow p-3 mb-5 bg-info rounded-3">
            <form className='row g-3'>
                <div className='col-md-6'>
                    <label htmlFor='firstName' className='form-label'>First Name</label>
                    <input type='text' className='form-control' id='firstName' name="firstName" value={props.newContact.first} onChange={props.handleFirst} />
                    {props.errors.first && <div className="text-danger">{props.errors.first}</div>}

                </div>

                <div className='col-md-6'>
                    <label htmlFor='lastName' className='form-label'>Last Name</label>
                    <input type='text' className='form-control' id='lastName' name="lastName" value={props.newContact.last} onChange={props.handleLast} />
                    {props.errors.last && <div className="text-danger">{props.errors.last}</div>}
                </div>

                <div className='col-md-6'>
                    <label htmlFor='email' className='form-label'>Email</label>
                    <input type='email' className='form-control' id='email' name="email" value={props.newContact.email} onChange={props.handleMail} />
                    {props.errors.email && <div className="text-danger">{props.errors.email}</div>}
                </div>

                <div className='col-md-6'>
                    <label htmlFor='phone' className='form-label'>Phone</label>
                    <input type='tel' className='form-control' id='phone' name="phone" value={props.newContact.phone} onChange={props.handlePhone} />
                    {props.errors.phone && <div className="text-danger">{props.errors.phone}</div>}
                </div>

                <div className='col-12'>
                    <button type='submit' className='btn btn-primary' onClick={props.handleSubmit}>Ajouter</button>
                </div>
            </form>
        </div>
    );
}

export default AddContact;