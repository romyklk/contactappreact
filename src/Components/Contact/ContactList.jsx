import { nanoid } from "nanoid";
import { useState } from "react";
import AddContact from "../Form/AddContact";
import Contact from "./Contact";

export default function ContactList() {
    const [contacts, setContacts] = useState([]);

    const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

    const frenchPhoneNumberRegex = /^(?:(?:(?:\+|00)33)|(?:0))[1-9](?:(?:(?:\d[ -]?){8})|(?:\d))$/;


    const [newContact, setNewContact] = useState({
        id: nanoid(8),
        first: '',
        last: '',
        email: '',
        phone: ''
    });

    const [errors, setErrors] = useState({
        first: '',
        last: '',
        email: '',
        phone: ''
    });

    const handleFirst = (e) => {
        const value = e.target.value;
        setNewContact({
            ...newContact,
            first: value
        });

        if (value.length < 3) {
            setErrors({
                ...errors,
                first: 'Le nom doit contenir au moins 3 caractères'
            });
        } else if (value.length > 30) {
            setErrors({
                ...errors,
                first: 'Le nom doit contenir au maximum 30 caractères'
            });
        } else {
            setErrors({
                ...errors,
                first: ''
            });
        }
    }

    const handleLast = (e) => {
        const value = e.target.value;
        setNewContact({
            ...newContact,
            last: value
        });

        if (value.length < 3) {
            setErrors({
                ...errors,
                last: 'Le nom doit contenir au moins 3 caractères'
            });
        } else if (value.length > 30) {
            setErrors({
                ...errors,
                last: 'Le nom doit contenir au maximum 30 caractères'
            });
        } else {
            setErrors({
                ...errors,
                last: ''
            });
        }
    }

    const handleMail = (e) => {
        const value = e.target.value;
        setNewContact({
            ...newContact,
            email: value
        });

        if (!value.includes('@')) {
            setErrors({
                ...errors,
                email: 'L\'email doit contenir un @'
            });
        } else if (value.length > 50) {
            setErrors({
                ...errors,
                email: 'L\'email doit contenir au maximum 50 caractères'
            });
        } else if (!value.includes('.')) {
            setErrors({
                ...errors,
                email: 'L\'email doit contenir un .'
            });
        } else if (!emailRegex.test(value)) {
            setErrors({
                ...errors,
                email: 'L\'email doit être valide'
            });
        } else {
            setErrors({
                ...errors,
                email: ''
            });
        }
    }

    const handlePhone = (e) => {
        const value = e.target.value;
        setNewContact({
            ...newContact,
            phone: value
        });

        if (value.length < 10) {
            setErrors({
                ...errors,
                phone: 'Le numéro doit contenir au moins 10 caractères'
            });
        /*}  else if (value.length > 10) {
            setErrors({
                ...errors,
                phone: 'Le numéro doit contenir au maximum 10 caractères'
            }); */
        } else if (isNaN(parseInt(value))) {
            setErrors({
                ...errors,
                phone: 'Le numéro doit contenir uniquement des chiffres'
            });
        } else if (!frenchPhoneNumberRegex.test(value)) {
            setErrors({
                ...errors,
                phone: 'Le numéro  de téléphone n\'est pas valide . Exemple : 0606060606'
            });
        } else {
            setErrors({
                ...errors,
                phone: ''
            });
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (newContact.first && newContact.last && newContact.email && newContact.phone && !errors.first && !errors.last && !errors.email && !errors.phone) {
            setContacts([...contacts, newContact]);
            setNewContact({
                id: nanoid(8),
                first: '',
                last: '',
                email: '',
                phone: ''
            });
        } else {
            alert('Veuillez remplir tous les champs');
        }
    }

    const handleDeleteContact = (id) => {
        return () => {
            if (window.confirm('Voulez-vous vraiment supprimer ce contact ?')) {
                setContacts(contacts.filter((contact) => contact.id !== id));
            }
        }
    }



    return (
        <div>
            <div className='mb-3'>
                <AddContact
                    handleFirst={handleFirst}
                    handleLast={handleLast}
                    handleMail={handleMail}
                    handlePhone={handlePhone}
                    handleSubmit={handleSubmit}
                    errors={errors}
                    newContact={newContact}
                />
            </div>

            <hr />

            <div className="table-responsive">
                <table className='table table-dark table-striped table-hover'>
                    <thead className="table-dark text-center">
                        <tr>
                            <th>Id</th>
                            <th>FirstName</th>
                            <th>LastName</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody className="text-center">
                        {contacts.length > 0 ? (
                            contacts.map((contact) => (
                                <Contact
                                    key={contact.id}
                                    id={contact.id}
                                    firstName={contact.first}
                                    lastName={contact.last}
                                    email={contact.email}
                                    phone={contact.phone}
                                    deleteContact={handleDeleteContact(contact.id)}
                                />
                            ))
                        ) : (
                            <tr>
                                <td colSpan={6}>
                                    <div className='alert alert-danger text-center'>
                                        <strong>
                                            Aucun contact pour le moment
                                        </strong>
                                    </div>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
