/* eslint-disable react/prop-types */

function Contact({ id, firstName, lastName, email, phone, deleteContact }) {

    return (
        <tr className="table-success" key={id} >
            <td>{id}</td>
            <td>
                {firstName}
            </td>
            <td>
                {lastName}
            </td>
            <td>
                {email}
            </td>
            <td>
                {phone}
            </td>
            <td>
                <button className='btn btn-sm btn-danger' onClick={deleteContact}>Delete</button>
            </td>
        </tr>
    )
}
export default Contact