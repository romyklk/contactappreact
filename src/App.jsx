import './App.css'
import ContactList from './Components/Contact/ContactList'


function App() {


  return (
    <>
      <div className='container-fluid vh-100' style={{ backgroundColor: '#040519', color:'#e7eaf9' }}>
        <div className='container'>
          <h1 className='text-center'>React Contact App</h1>
          <ContactList />
        </div>
      </div>
    </>
  )
}

export default App
