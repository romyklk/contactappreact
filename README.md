Voici un autre TP qui vous permettra de continuer à explorer les concepts de props, state et le passage d'éléments entre composants en React :

Ceci est le lien pour voir le rendu de l'application : [contactApp](https://contactappreact.vercel.app/)

![Contact App Screen](/src/assets/app.png)


**Créer une liste de contacts :**

Objectif : Créer une liste de contacts où vous pouvez ajouter, afficher et supprimer des contacts.

Instructions :

1. Commencez par créer un nouveau projet React si vous n'en avez pas déjà un avec vitejs
   `vite create my-app --template react`

2. Créez un composant `ContactList` qui sera responsable de l'affichage de la liste de contacts.

3. Créez un composant `Contact` pour représenter un contact individuel. Ce composant doit accepter des props telles que `nom`, `email`, et `téléphone` pour afficher les détails du contact.

4. Dans le composant `ContactList`, créez un state pour stocker la liste des contacts. Initialisez-le avec quelques contacts par défaut.

5. Affichez la liste de contacts en utilisant le composant `Contact` dans le composant `ContactList`. Utilisez une méthode de rendu de liste (par exemple, `map`) pour itérer sur les contacts et les passer comme props au composant `Contact`.

6. Ajoutez un formulaire dans le composant `ContactList` qui permet aux utilisateurs d'ajouter de nouveaux contacts. Utilisez le state pour suivre les nouveaux contacts en cours d'ajout et mettez à jour le state lorsque le formulaire est soumis.

7. Ajoutez un bouton de suppression à chaque contact (composant `Contact`). Lorsque ce bouton est cliqué, supprimez le contact correspondant du state dans le composant `ContactList`.

8. Testez votre application en ajoutant et en supprimant des contacts.

Ce TP vous permettra de renforcer votre compréhension de la gestion de props et de state dans React, ainsi que de la communication entre les composants. Vous apprendrez également à créer un formulaire pour ajouter des données et à mettre à jour le state en conséquence.